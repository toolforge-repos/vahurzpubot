# All code here copyright 2020-2024 by User:Vahurzpu. Released under the Apache
# License v2.0

### Notes about the bot ###

# The query that selects articles does leave soft redirects alone; these and
# non-WPBannerMeta WikiProject templates are the only pages that *need* explicit
# Redirect-Class designations.

# Some redirect-class articles have the B-class criteria filled out. I'm not
# sure exactly how you could have B-class criteria assessed for a redirect, but
# I leave them alone here.

# In addition to clearing out the redirect-class, it also clears out any
# "importance=na", as this isn't applicable to articles. Sometimes the
# importance=na will still be true (i.e. for disambiguation pages), but that
# will be added whenever someone re-assesses the page as a disambiguation.

import os
import pywikibot
import mwparserfromhell as mwp
import difflib
import random
import time
import toolforge

conn = toolforge.connect('enwiki')
# See https://quarry.wmflabs.org/query/49607 for a quick look at what this returns
query = """
SELECT DISTINCT mainpage.page_title
FROM categorylinks outercat
INNER JOIN page outerpage ON outerpage.page_id = outercat.cl_from
INNER JOIN categorylinks innercat ON outerpage.page_title = innercat.cl_to
INNER JOIN page talkpage ON talkpage.page_id = innercat.cl_from
INNER JOIN page mainpage ON mainpage.page_title = talkpage.page_title
WHERE outercat.cl_to = "Redirect-Class_articles"
AND talkpage.page_namespace = 1
AND mainpage.page_namespace = 0
AND mainpage.page_is_redirect = 0
AND mainpage.page_id NOT IN (
  SELECT innersoftcat.cl_from
  FROM categorylinks outersoftcat
  INNER JOIN page softcatpage ON softcatpage.page_id = outersoftcat.cl_from
  INNER JOIN categorylinks innersoftcat ON innersoftcat.cl_to = softcatpage.page_title
  WHERE outersoftcat.cl_to = "Wikipedia_interwiki_soft_redirects"
  AND softcatpage.page_namespace = 14
)
AND "Temporary_maintenance_holdings" NOT IN (SELECT cl_to FROM categorylinks WHERE cl_from = mainpage.page_id)
AND "Requested_RD1_redactions" NOT IN (SELECT cl_to FROM categorylinks WHERE cl_from = mainpage.page_id)
""".strip()

with conn.cursor() as cur:
    cur.execute(query)
    data = cur.fetchall()

enwiki = pywikibot.Site('en', 'wikipedia')
pages_to_edit = [pywikibot.Page(enwiki, 'Talk:' + x[0].decode('utf-8')) for x in data]

assessment_templates = set(pywikibot.Category(enwiki, 'WikiProject banners with quality assessment').articles())
# Add one non-WPBannerMeta-based WikiProject banner manually; it uses the
# relevant parameters in the same fashion, so this shouldn't cause any issues
assessment_templates.add(pywikibot.Page(enwiki, 'Template:WikiProject UK Roads'))

# Now that [[WP:PIQA]] has been implemented, the banner shell can also have a class
assessment_templates.add(pywikibot.Page(enwiki, 'Template:WikiProject banner shell'))

not_assessment_templates = {pywikibot.Page(enwiki, 'Template:Talk header')}

def is_assessment_template(template):
    template_name = str(template.name)
    if template_name.lower().startswith('talk:') or template_name.lower().startswith(':talk:'):
        return False
    if template_name.startswith('#'): # actually a parser function
        return False
    if template_name.startswith('fullurl:'): # magic word
        return False
    if not template_name.lower().startswith('template:'):
        template_name = 'Template:' + template_name
    template_page = pywikibot.Page(enwiki, template_name)
    if template_page in assessment_templates:
        return True
    if template_page in not_assessment_templates:
        return False
    # If we're here, it's not in either known set
    if template_page.isRedirectPage():
        new_template_page = template_page.getRedirectTarget()
        if new_template_page in assessment_templates:
            assessment_templates.add(template_page)
            return True
        else:
            not_assessment_templates.add(template_page)
            not_assessment_templates.add(new_template_page)
            return False
    else:
        not_assessment_templates.add(template_page)
        return False

redirect_aliases = {'redirect', 'redir', 'red', 'rdr'}

def clear_assessment_template(template):
    if template.has('class'):
        class_param = template.get('class')
        if str(class_param.value).lower().strip() in redirect_aliases:
            class_param.value = ''
            for param in template.params:
                # endswith to capture subproject importances (e.g. MA-importance= for WPUS)
                if str(param.name).lower().endswith('importance') and str(param.value).lower().strip() == 'na':
                    param.value = ''

def update_text(raw_text):
    wikitext = mwp.parse(raw_text)
    for template in wikitext.filter_templates():
        if is_assessment_template(template):
            clear_assessment_template(template)
    return str(wikitext)

def execute_edit(page):
    if page.botMayEdit():
        page.text = update_text(page.text)
        page.save('[[Wikipedia:Bots/Requests for approval/VahurzpuBot|Task 1]]: Remove redirect-class assessment from non-redirect')

for page in pages_to_edit:
    execute_edit(page)
    time.sleep(10)

