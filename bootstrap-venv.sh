#!/bin/bash

# use bash strict mode
set -euo pipefail

python3 -m venv .env
source .env/bin/activate
pip install -U pip wheel

pip install toolforge mwparserfromhell pywikibot[mwoauth]
